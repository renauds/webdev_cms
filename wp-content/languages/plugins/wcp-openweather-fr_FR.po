# Translation of Plugins - WCP OpenWeather - Development (trunk) in French (France)
# This file is distributed under the same license as the Plugins - WCP OpenWeather - Development (trunk) package.
msgid ""
msgstr ""
"PO-Revision-Date: 2021-05-03 11:00:34+0000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: GlotPress/3.0.0-alpha.2\n"
"Language: fr\n"
"Project-Id-Version: Plugins - WCP OpenWeather - Development (trunk)\n"

#: config/admin-options-fields.php:188
msgid "Google API key"
msgstr "Clé d’API Google"

#: classes/RPw.class.php:558
msgid "Google API key is required. Please, enter valid Google API Key in the \"API\" tab."
msgstr "La clé d’API Google est obligatoire. Veuillez saisir une clé d’API Google valide dans l’onglet « API »."

#: templates/admin/options/layout-headline-links.php:8
msgid "Help with Translation"
msgstr "Aider à la traduction"

#: templates/admin/options/layout-headline-links.php:7
msgid "Support Form"
msgstr "Formulaire de support"

#: templates/admin/options/layout-headline-links.php:6
msgid "FAQ"
msgstr "FAQ"

#: templates/admin/options/layout-headline-links.php:5
msgid "Live Demo"
msgstr "Démo en direct"

#: templates/admin/options/layout-headline-links.php:5
msgid "Documentation"
msgstr "Documentation"

#: templates/admin/options/layout-headline-links.php:3
msgid "Useful Links"
msgstr "Liens utiles"

#: classes/RPw.class.php:223
msgid "Add new WCP OpenWeather shortcode"
msgstr "Ajouter un nouveau WCP OpenWeather shortcode"

#: classes/Settings.class.php:110
msgid "Auto Detect"
msgstr "Détection Automatique"

#: classes/Settings.class.php:118
msgid "Default"
msgstr "Par Défaut"

#: classes/widget/WeatherWidgetAbstract.class.php:117
msgid "Title"
msgstr "Titre"

#: classes/widget/WeatherWidgetAbstract.class.php:126
#: config/admin-options-fields.php:69
msgid "Template"
msgstr "Modèle"

#: classes/widget/WeatherWidgetAbstract.class.php:143
#: config/admin-options-fields.php:77
msgid "Show current weather"
msgstr "Afficher la météo actuelle"

#: classes/widget/WeatherWidgetAbstract.class.php:150
#: config/admin-options-fields.php:84
msgid "Show 5 day forecast"
msgstr "Voir prévisions à 5 jours"

#: classes/widget/WeatherWidgetAbstract.class.php:156
#: config/admin-options-fields.php:27
msgid "City Name"
msgstr "Nom De La Ville"

#: classes/widget/WeatherWidgetAbstract.class.php:158
#: config/admin-options-fields.php:33
msgid "You can find you city name on <a href=\"http://www.openweathermap.com/\" title=\"http://www.openweathermap.com/\" target=\"_blank\">www.openweathermap.com</a>."
msgstr "Vous pouvez vous trouver le nom de la ville sur <a href=\"http://www.openweathermap.com/\" title=\"http://www.openweathermap.com/\" target=\"_blank\">www.openweathermap.com</a>."

#: classes/widget/WeatherWidgetAbstract.class.php:174
#: config/admin-options-fields.php:45
msgid "Temperature"
msgstr "Température"

#: classes/widget/WeatherWidgetAbstract.class.php:193
#: config/admin-options-fields.php:53
msgid "Wind Speed"
msgstr "Vitesse du vent"

#: classes/widget/WeatherWidgetAbstract.class.php:212
#: config/admin-options-fields.php:61
#: theme/default/templates/shortcode/wcp_weather/compact/now.php:38
#: theme/default/templates/shortcode/wcp_weather/default/forecast.php:16
#: theme/default/templates/shortcode/wcp_weather/default/now.php:38
#: theme/metro/templates/shortcode/wcp_weather/compact/now.php:19
#: theme/metro/templates/shortcode/wcp_weather/default/forecast.php:16
#: theme/metro/templates/shortcode/wcp_weather/default/now.php:19
#: theme/metro/templates/widget/wcp_weather_widget/default/now.php:25
msgid "Pressure"
msgstr "Pression"

#: classes/widget/WeatherWidgetAbstract.class.php:228
msgid "Enable user options"
msgstr "Activer les options de l'utilisateur"

#: classes/widget/WeatherWidgetAbstract.class.php:235
msgid "Hide description of the weather conditions"
msgstr "Cacher la description des conditions météorologiques"

#: classes/persistence/Convertor.class.php:104
#: classes/persistence/Convertor.class.php:120
msgid "N"
msgstr "N"

#: classes/persistence/Convertor.class.php:105
msgid "NNE"
msgstr "NNE"

#: classes/persistence/Convertor.class.php:106
msgid "NE"
msgstr "NE"

#: classes/persistence/Convertor.class.php:107
msgid "ENE"
msgstr "ENE"

#: classes/persistence/Convertor.class.php:108
msgid "E"
msgstr "E"

#: classes/persistence/Convertor.class.php:109
msgid "ESE"
msgstr "ESE"

#: classes/persistence/Convertor.class.php:110
msgid "SE"
msgstr "SE"

#: classes/persistence/Convertor.class.php:111
msgid "SSE"
msgstr "SSE"

#: classes/persistence/Convertor.class.php:112
msgid "S"
msgstr "S"

#: classes/persistence/Convertor.class.php:113
msgid "SSW"
msgstr "SSO"

#: classes/persistence/Convertor.class.php:114
msgid "SW"
msgstr "SO"

#: classes/persistence/Convertor.class.php:115
msgid "WSW"
msgstr "OSO"

#: classes/persistence/Convertor.class.php:116
msgid "W"
msgstr "O"

#: classes/persistence/Convertor.class.php:117
msgid "WNW"
msgstr "ONO"

#: classes/persistence/Convertor.class.php:118
msgid "NW"
msgstr "NO"

#: classes/persistence/Convertor.class.php:119
msgid "NNW"
msgstr "NNO"

#: classes/theme/ThemeSettings.class.php:114
msgid "Settings reset to default values"
msgstr "Réglages de réinitialisation aux valeurs par défaut"

#: classes/theme/ThemeEntity.class.php:148
#: theme/metro/templates/admin/constructor/addon.php:9
msgid "Theme Settings"
msgstr "Paramètres thème"

#: classes/theme/ThemeEntity.class.php:162
msgid "Select"
msgstr "Choisir"

#: classes/theme/ThemeEntity.class.php:163
#: theme/metro/classes/widget/abstract/ThemeWeatherWidget.class.php:52
#: theme/metro/templates/admin/constructor/fields/shortcodeImage.php:31
#: theme/metro/templates/admin/options/fields/image.php:39
msgid "Upload Image"
msgstr "Envoyer une image"

#: config/admin-options-fields.php:6
msgid "Location"
msgstr "Emplacement"

#: config/admin-options-fields.php:9
msgid "Units"
msgstr "Unités"

#: config/admin-options-fields.php:13
msgid "Display Options"
msgstr "Options d'Affichage"

#: config/admin-options-fields.php:94
msgid "General"
msgstr "Principal"

#: config/admin-options-fields.php:97
msgid "User Options"
msgstr "Options utilisateur"

#: config/admin-options-fields.php:100
msgid "Other"
msgstr "Autre"

#: config/admin-options-fields.php:106
msgid "Language"
msgstr "Langue"

#: config/admin-options-fields.php:115
msgid "Current Theme"
msgstr "Thème Actuel"

#: config/admin-options-fields.php:124
msgid "Refresh Time"
msgstr "Actualiser Temps"

#: config/admin-options-fields.php:131
msgid "Hide Description of the Weather Conditions"
msgstr "Cacher la description des conditions météorologiques"

#: config/admin-options-fields.php:138
msgid "\"No Data\" Message"
msgstr "Message \"Aucune Donnée\""

#: config/admin-options-fields.php:146
msgid "Enable \"Google Maps API\""
msgstr "Activer les \"Google Maps API\""

#: config/admin-options-fields.php:153
msgid "Enable User Options"
msgstr "Activer les options de l'utilisateur"

#: config/admin-options-fields.php:160
msgid "Expire User Options (days)"
msgstr "Expirez Options Utilisateur (jours)"

#: config/admin-options-fields.php:185
msgid "How to get API key you can see <a href=\"http://openweathermap.org/appid\" title=\"http://openweathermap.org/appid\" target=\"_blank\">here</a>."
msgstr "Comment faire pour obtenir la clé API que vous pouvez voir <a href=\"http://openweathermap.org/appid\" title=\"http://openweathermap.org/appid\" target=\"_blank\">ici</a>."

#: config/admin-options-tabs.php:4
msgid "Weather"
msgstr "Météo"

#: config/admin-options-tabs.php:7
msgid "Plugin"
msgstr "Extension"

#: config/admin-options-tabs.php:10
msgid "API"
msgstr "API"

#: config/admin-menu.php:5 config/admin-menu.php:6
#: theme/default/classes/widget/Weather.class.php:13
#: theme/metro/classes/widget/Weather.class.php:11
msgid "WCP Weather"
msgstr "WCP Weather"

#: config/admin-menu.php:14 config/admin-menu.php:15
msgid "Settings"
msgstr "Paramètres"

#: config/admin-options-fieldset.php:4
msgid "&deg;C"
msgstr "&deg;C"

#: config/admin-options-fieldset.php:5
msgid "&deg;F"
msgstr "&deg;F"

#: config/admin-options-fieldset.php:8
msgid "mph"
msgstr "mph"

#: config/admin-options-fieldset.php:9
msgid "km/h"
msgstr "km/h"

#: config/admin-options-fieldset.php:10
msgid "m/s"
msgstr "m/s"

#: config/admin-options-fieldset.php:11
msgid "Knots"
msgstr "Knots"

#: config/admin-options-fieldset.php:14
msgid "atm"
msgstr "atm"

#: config/admin-options-fieldset.php:15
msgid "bar"
msgstr "bar"

#: config/admin-options-fieldset.php:16
msgid "hPa"
msgstr "hPa"

#: config/admin-options-fieldset.php:17
msgid "kgf/cm²"
msgstr "kgf/cm²"

#: config/admin-options-fieldset.php:18
msgid "kgf/m²"
msgstr "kgf/m²"

#: config/admin-options-fieldset.php:19
msgid "kPa"
msgstr "kPa"

#: config/admin-options-fieldset.php:20
msgid "mbar"
msgstr "mbar"

#: config/admin-options-fieldset.php:21
msgid "mmHg"
msgstr "mmHg"

#: config/admin-options-fieldset.php:22
msgid "inHg"
msgstr "inHg"

#: config/admin-options-fieldset.php:23
msgid "Pa"
msgstr "Pa"

#: config/admin-options-fieldset.php:24
msgid "psf"
msgstr "psf"

#: config/admin-options-fieldset.php:25
msgid "psi"
msgstr "psi"

#: config/admin-options-fieldset.php:26
msgid "torr"
msgstr "torr"

#: config/admin-options-fieldset.php:29
msgid "Always"
msgstr "Toujours"

#: config/admin-options-fieldset.php:30
msgid "0.5h - Not Recommended"
msgstr "0.5h - Déconseillé"

#: config/admin-options-fieldset.php:31
msgid "1h - Recommended"
msgstr "1h - Recommandé"

#: config/admin-options-fieldset.php:32
msgid "2h"
msgstr "2h"

#: config/admin-options-fieldset.php:33
msgid "3h"
msgstr "3h"

#: config/admin-options-fieldset.php:34
msgid "6h"
msgstr "6h"

#: config/admin-options-fieldset.php:35
msgid "9h"
msgstr "9h"

#: config/admin-options-fieldset.php:36
msgid "12h"
msgstr "12h"

#: config/admin-options-fieldset.php:37
msgid "24h"
msgstr "24h"

#: config/admin-options-fieldset.php:71
msgid "mon"
msgstr "lun"

#: config/admin-options-fieldset.php:72
msgid "tue"
msgstr "mar"

#: config/admin-options-fieldset.php:73
msgid "wed"
msgstr "mer"

#: config/admin-options-fieldset.php:74
msgid "thu"
msgstr "jeu"

#: config/admin-options-fieldset.php:75
msgid "fri"
msgstr "ven"

#: config/admin-options-fieldset.php:76
msgid "sat"
msgstr "sam"

#: config/admin-options-fieldset.php:77
msgid "sun"
msgstr "dim"

#: config/admin-options-fieldset.php:80
msgid "jan"
msgstr "janv"

#: config/admin-options-fieldset.php:81
msgid "feb"
msgstr "févr"

#: config/admin-options-fieldset.php:82
msgid "mar"
msgstr "mars"

#: config/admin-options-fieldset.php:83
msgid "apr"
msgstr "avril"

#: config/admin-options-fieldset.php:84
msgid "may"
msgstr "mai"

#: config/admin-options-fieldset.php:85
msgid "jun"
msgstr "juin"

#: config/admin-options-fieldset.php:86
msgid "jul"
msgstr "juil"

#: config/admin-options-fieldset.php:87
msgid "aug"
msgstr "août"

#: config/admin-options-fieldset.php:88
msgid "sep"
msgstr "sept"

#: config/admin-options-fieldset.php:89
msgid "oct"
msgstr "oct"

#: config/admin-options-fieldset.php:90
msgid "nov"
msgstr "nov"

#: config/admin-options-fieldset.php:91
msgid "dec"
msgstr "déc"

#: config/admin-options-fields.php:175 templates/user/options/layout.php:12
msgid "Options"
msgstr "Options"

#: templates/user/options/layout.php:20
msgid "Save & Refresh"
msgstr "Sauvegarder & Refresh"

#: templates/admin/options/layout.php:46 templates/user/options/layout.php:21
#: theme/default/templates/admin/options/layout.php:44
#: theme/metro/templates/admin/options/layout.php:44
msgid "Reset to Default"
msgstr "Réinitialiser"

#: templates/admin/options/layout.php:45
#: theme/default/templates/admin/options/layout.php:43
#: theme/metro/templates/admin/options/layout.php:43
msgid "Save Changes"
msgstr "Sauvegarder Les Changements"

#: templates/admin/options/fields/lang.php:46
msgid "Not supported in the current theme"
msgstr "Non pris en charge dans le thème actuel"

#: templates/admin/constructor/layout.php:12
msgid "WCP OpenWeather Shortcode"
msgstr "WCP OpenWeather Shortcode"

#: templates/admin/constructor/layout.php:27
msgid "Add"
msgstr "Ajouter"

#: theme/default/classes/Theme.class.php:42
msgid "Default Theme"
msgstr "Thème Par Défaut"

#: theme/default/classes/widget/Weather.class.php:12
#: theme/metro/classes/widget/Weather.class.php:10
msgid "Adds weather to sidebar"
msgstr "Ajoute météo à sidebar"

#: theme/default/classes/widget/WeatherMini.class.php:12
#: theme/metro/classes/widget/WeatherMini.class.php:10
msgid "Adds mini weather to sidebar"
msgstr "Ajoute un mini météo to sidebar"

#: theme/default/classes/widget/WeatherMini.class.php:13
#: theme/metro/classes/widget/WeatherMini.class.php:11
msgid "WCP Weather Mini"
msgstr "WCP Weather Mini"

#: theme/default/config/admin-options-fields.php:12
#: theme/metro/classes/widget/abstract/ThemeWeatherWidget.class.php:77
#: theme/metro/config/admin-options-fields.php:28
msgid "Background Color"
msgstr "Couleur De Fond"

#: theme/default/config/admin-options-fields.php:22
#: theme/metro/classes/widget/abstract/ThemeWeatherWidget.class.php:63
#: theme/metro/config/admin-options-fields.php:18
msgid "Text Color"
msgstr "Couleur Du Texte"

#: theme/default/templates/shortcode/wcp_weather/compact/nodata.php:5
#: theme/default/templates/shortcode/wcp_weather/default/nodata.php:5
#: theme/default/templates/widget/wcp_weather_mini_widget/default/nodata.php:5
#: theme/default/templates/widget/wcp_weather_widget/compact/nodata.php:5
#: theme/default/templates/widget/wcp_weather_widget/default/nodata.php:5
#: theme/metro/templates/shortcode/wcp_weather/compact/nodata.php:5
#: theme/metro/templates/shortcode/wcp_weather/default/nodata.php:5
#: theme/metro/templates/widget/wcp_weather_mini_widget/default/nodata.php:5
#: theme/metro/templates/widget/wcp_weather_widget/compact/nodata.php:5
#: theme/metro/templates/widget/wcp_weather_widget/default/nodata.php:5
msgid "Ooops! Nothing was found!"
msgstr "Rien n'a été trouvé!"

#: theme/default/templates/shortcode/wcp_weather/default/forecast.php:11
#: theme/default/templates/widget/wcp_weather_mini_widget/default/forecast.php:11
#: theme/metro/templates/shortcode/wcp_weather/default/forecast.php:11
#: theme/metro/templates/widget/wcp_weather_mini_widget/default/forecast.php:11
msgid "Day"
msgstr "Jour"

#: theme/default/templates/shortcode/wcp_weather/default/forecast.php:12
#: theme/default/templates/widget/wcp_weather_mini_widget/default/forecast.php:12
#: theme/metro/templates/shortcode/wcp_weather/default/forecast.php:12
#: theme/metro/templates/widget/wcp_weather_mini_widget/default/forecast.php:12
msgid "Cond."
msgstr "Cond."

#: theme/default/templates/shortcode/wcp_weather/default/forecast.php:13
#: theme/default/templates/widget/wcp_weather_mini_widget/default/forecast.php:13
#: theme/metro/templates/shortcode/wcp_weather/default/forecast.php:13
#: theme/metro/templates/widget/wcp_weather_mini_widget/default/forecast.php:13
msgid "Temp."
msgstr "Temp."

#: theme/default/templates/shortcode/wcp_weather/compact/now.php:26
#: theme/default/templates/shortcode/wcp_weather/default/forecast.php:14
#: theme/default/templates/shortcode/wcp_weather/default/now.php:26
#: theme/metro/templates/shortcode/wcp_weather/compact/now.php:13
#: theme/metro/templates/shortcode/wcp_weather/default/forecast.php:14
#: theme/metro/templates/shortcode/wcp_weather/default/now.php:13
#: theme/metro/templates/widget/wcp_weather_widget/default/now.php:21
msgid "Wind"
msgstr "Vent"

#: theme/default/templates/shortcode/wcp_weather/compact/now.php:32
#: theme/default/templates/shortcode/wcp_weather/default/forecast.php:15
#: theme/default/templates/shortcode/wcp_weather/default/now.php:32
#: theme/metro/templates/shortcode/wcp_weather/compact/now.php:36
#: theme/metro/templates/shortcode/wcp_weather/default/forecast.php:15
#: theme/metro/templates/shortcode/wcp_weather/default/now.php:36
#: theme/metro/templates/widget/wcp_weather_widget/default/now.php:31
msgid "Humidity"
msgstr "Humidité"

#: theme/default/templates/shortcode/wcp_weather/default/forecast.php:16
#: theme/metro/templates/shortcode/wcp_weather/default/forecast.php:16
msgid "Pres."
msgstr "Pres."

#: theme/default/templates/admin/options/layout.php:22
#: theme/metro/templates/admin/options/layout.php:22
msgid "You can change current theme <a href=\"/wp-admin/admin.php?page=wcp-weather&tab=plugin-settings\">here</a>."
msgstr "Vous pouvez changer de thème courant <a href=\"/wp-admin/admin.php?page=wcp-weather&tab=plugin-settings\">ici</a>."

#: theme/metro/classes/Theme.class.php:42
msgid "Metro Theme"
msgstr "Metro Thème"

#: theme/metro/classes/widget/abstract/ThemeWeatherWidget.class.php:43
#: theme/metro/config/admin-options-fields.php:61
#: theme/metro/config/admin-options-fields.php:81
msgid "Image"
msgstr "Image"

#: theme/metro/classes/widget/abstract/ThemeWeatherWidget.class.php:47
#: theme/metro/templates/admin/constructor/fields/shortcodeImage.php:26
#: theme/metro/templates/admin/options/fields/image.php:34
msgid "Delete Image"
msgstr "Supprimer image"

#: theme/metro/classes/widget/abstract/ThemeWeatherWidget.class.php:92
#: theme/metro/config/admin-options-fields.php:38
msgid "Background Opacity, %"
msgstr "Opacité de fond, %"

#: theme/default/config/admin-options-fields.php:6
#: theme/metro/config/admin-options-fields.php:6
msgid "Global settings"
msgstr "Paramètres globaux"

#: theme/metro/config/admin-options-fields.php:9
msgid "Widget settings"
msgstr "Paramètres widget"

#: theme/metro/config/admin-options-fields.php:12
msgid "Shortcode settings"
msgstr "Paramètres du shortcode"

#: theme/metro/templates/shortcode/wcp_weather/compact/now.php:42
#: theme/metro/templates/shortcode/wcp_weather/default/now.php:42
#: theme/metro/templates/widget/wcp_weather_widget/default/now.php:35
msgid "Clouds"
msgstr "Nuages"

#: theme/metro/templates/admin/options/fields/imagesize.php:23
msgid "Width"
msgstr "Largeur"

#: theme/metro/templates/admin/options/fields/imagesize.php:25
msgid "Height"
msgstr "Hauteur"

#: theme/metro/templates/admin/options/fields/imagesize.php:29
msgid "Crop thumbnail to exact dimensions"
msgstr "Crop miniature aux dimensions exactes"

#: theme/metro/templates/admin/constructor/before-render.php:5
msgid "Use default plugin settings"
msgstr "Paramètres de plug-in Utilisation par défaut"

#: theme/metro/config/admin-options-fields.php:49
#: theme/metro/config/admin-options-fields.php:69
msgid "Image Size"
msgstr "Taille de l’image"

#. Author URI of the plugin/theme
msgid "https://profiles.wordpress.org/webcodin/"
msgstr "https://profiles.wordpress.org/webcodin/"

#. Author of the plugin/theme
msgid "Webcodin"
msgstr "Webcodin"

#. Plugin URI of the plugin/theme
msgid "https://wordpress.org/plugins/wcp-openweather/"
msgstr "https://wordpress.org/plugins/wcp-openweather/"

#. Plugin Name of the plugin/theme
msgid "WCP OpenWeather"
msgstr "WCP OpenWeather"

#: config/admin-options-fields.php:180
msgid "API key"
msgstr "Clé de l’API"