��    F      L  a   |         1        3  	   Q     [     i     �     �     �     �      �     �  
   �     �  +   �          (  �   -     �     �     �     �  
   �     �     �     	                    <  1   C     u     �     �     �     �     �     �     �     �     
	     	     "	  	   )	     3	     <	  .   A	  	   p	     z	     �	  N   �	     �	     �	     �	  	   �	     �	     
     
     #
     <
     Q
     h
     |
  $   �
     �
     �
  "   �
          &     <  �  P  ?   �          /     <  $   U     z  
   �     �     �  (   �     �     �     �  ,   �     '     >  �   G     �     �     �            	        (     @     G  	   O  .   Y     �  /   �     �     �  "   �               .     ;     C  .   T     �     �     �     �     �  
   �  9   �  	         
       a        x     �     �     �     �  	   �     �     �     �               -  $   J     o     �  "   �     �     �     �     8   $           F                            )          6   7         &      4                 .   E   -   ?   #          D       :          =   ;       @            (   A      /       5         B   *       	   !              "   9          '          0                            >   %      1          ,      3   
       <   +       C       2              %s exceeds the maximum upload size for this site. <code>jpg jpeg png gif</code> Alignment Allowed Files Automatically add paragraphs Blank Caption Center Choose Image Crop avatars to exact dimensions Custom Custom URL Description: Disable Gravatar and use only local avatars Dismiss this notice Edit For users without a custom avatar of their own, you can either display a generic logo or a generated one based on their e-mail address. Gravatar Logo Height Identicon (Generated) Image Image File Insert Insert into Post Large Left Link To Maximum upload file size: %d%s. Medium Memory exceeded. Please try another smaller file. MonsterID (Generated) Mystery Man Open link in a new window Original Size Profile updated. ProfilePress Remove Remove Image Resize avatars on upload Retro (Generated) Right Search Select %s Settings Size This file is not an image. Please try another. Thumbnail Title: URL Unable to create directory %s. Is its parent directory writable by the server? Undo Update Profile Upload User Name Wavatar (Generated) Width [avatar_upload] https://profilepress.net wp_user_avatar_align wp_user_avatar_caption wp_user_avatar_link wp_user_avatar_link_external wp_user_avatar_link_external_section wp_user_avatar_size wp_user_avatar_size_number wp_user_avatar_size_number_section wp_user_avatar_target wp_user_avatar_upload wp_user_avatar_user PO-Revision-Date: 2021-05-16 13:44:43+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: it
Project-Id-Version: Plugins - User Registration, Login Form, User Profile &amp; Membership – ProfilePress (Formerly WP User Avatar) - Stable (latest release)
 %s supera le dimensioni massime di caricamento per questo sito. <code>jpg jpeg png gif</code> Allineamento Tipi di files consentiti Aggiungi i paragrafi automaticamente Vuoto Didascalia Centra Scegli immagine Taglia gli avatar alle dimensioni esatte Personalizzato Url personalizzato Descrizione: Disabilita Gravatar e usa solo avatar locali Ignora questa notifica Modifica Per gli utenti senza un avatar personalizzato, è possibile visualizzare un logo generico o uno generato in base all'indirizzo di posta elettronica. Gravatar Logo Altezza Identicon (Generato) Immagine File Immagine Inserisci Inserisci nell'articolo Grande Sinista Collega a Dimensione massima del file da caricare: %d%s. Medio File troppo grande. Devi ridurre le dimensioni. MonsterID (Generato) Utente Anonimo Apri il link in una nuova finestra Dimensioni Originali Profilo aggiornato. ProfilePress Rimuovi Rimuovi immagine Ridimensiona gli avatar durante il caricamento Retro (Generato) Destra Cerca Seleziona %s Impostazioni Dimensione Questo file non è un' immagine. Prova con un altro file. Miniatura Titolo: URL Impossibile creare la cartella %s. Controllare che la cartella superiore non sia in sola lettura. Annulla Aggiorna il Profilo Carica Nome Utente Wavatar (Generato) Larghezza [avatar_upload] https://profilepress.net wp_user_avatar_align wp_user_avatar_caption wp_user_avatar_link wp_user_avatar_link_external wp_user_avatar_link_external_section wp_user_avatar_size wp_user_avatar_size_number wp_user_avatar_size_number_section wp_user_avatar_target wp_user_avatar_upload wp_user_avatar_user 