msgid ""
msgstr ""
"Project-Id-Version: OWM Weather\n"
"Report-Msgid-Bugs-To: http://wordpress.org/support/plugin/owm-weather\n"
"POT-Creation-Date: 2017-09-19 19:10+0200\n"
"PO-Revision-Date: 2017-09-19 19:10+0200\n"
"Last-Translator: \n"
"Language-Team: contact@wpcloudy.com\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.0.1\n"
"X-Poedit-KeywordsList: __;_e;_x\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-SourceCharset: UTF-8\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: js\n"

#: owmweather-admin.php:64
msgid "The best freemium plugin to boost your SEO with WordPress."
msgstr ""

#: owmweather-admin.php:65
msgid "Download for free"
msgstr ""

#: owmweather-admin.php:69
msgid "OWM Weather"
msgstr "OWM Weather"

#: owmweather-admin.php:75
msgid "You like OWM Weather? Don't forget to rate it 5 stars!"
msgstr "‎אהבת את התוסף‫?‬ אל תשכח לדרג אותו 5 כוכבים‫!‬"

#: owmweather-admin.php:112
msgid "Export Settings"
msgstr "‎ייצא הגדרות"

#: owmweather-admin.php:114
msgid ""
"Export the plugin settings for this site as a .json file. This allows you to "
"easily import the configuration into another site."
msgstr ""
"‎ייצא את הגדרות התוסף לקובץ .json‫.‬ זה יאפשר לך לייבא את ההגדרות לאתר אחר‫.‬"

#: owmweather-admin.php:119
msgid "Export"
msgstr "‎ייצא"

#: owmweather-admin.php:126
msgid "Import Settings"
msgstr "‎ייבא הגדרות"

#: owmweather-admin.php:128
msgid ""
"Import the plugin settings from a .json file. This file can be obtained by "
"exporting the settings on another site using the form above."
msgstr ""
"‏‎ייבא את הגדרות התוסף לקובץ .json‫.‬ ניתן להשיג קובץ זה על ידי ייצוא ההגדרות "
"מאתר אחר באמצעות הטופס שמופיע למעלה."

#: owmweather-admin.php:136
msgid "Import"
msgstr "‎ייבא"

#: owmweather-admin.php:143
msgid "Reset Settings"
msgstr ""

#: owmweather-admin.php:145
msgid ""
"Reset all OWM Weather global settings. It will not delete your weathers and "
"their indivuals settings."
msgstr ""

#: owmweather-admin.php:150
msgid "Reset settings"
msgstr ""

#: owmweather-admin.php:169
msgid "Basic"
msgstr "בסיסי"

#: owmweather-admin.php:170 owmweather.php:391
msgid "Display"
msgstr "‎תצוגה"

#: owmweather-admin.php:171 owmweather.php:392
msgid "Advanced"
msgstr "‎מתקדם"

#: owmweather-admin.php:172 owmweather.php:393
msgid "Map"
msgstr "‎מפה"

#: owmweather-admin.php:173
msgid "Import/Export/Reset"
msgstr ""

#: owmweather-admin.php:174
msgid "Support"
msgstr "‎תמיכה"

#: owmweather-admin.php:188
msgid "Save changes"
msgstr "‎שמור שינויים"

#: owmweather-admin.php:193
msgid "OWM Weather Premium"
msgstr ""

#: owmweather-admin.php:197
msgid "$ 19 / year"
msgstr ""

#: owmweather-admin.php:199
msgid "Enabled"
msgstr "‎פועל"

#: owmweather-admin.php:203
msgid ""
"Join now to get Skins & Geolocation Add-ons, and premium support by email."
msgstr ""

#: owmweather-admin.php:207
msgid "Join now"
msgstr ""

#: owmweather-admin.php:211
msgid "OWM Weather cache"
msgstr "‎מטמון OWM Weather"

#: owmweather-admin.php:215
msgid "cache system"
msgstr "‎מטמון מערכת"

#: owmweather-admin.php:218
msgid "Click this button to refresh weather cache."
msgstr "‎לחץ על הכפתור הזה כדי לרענן את מטמון מזג האוויר‫.‬"

#: owmweather-admin.php:276 owmweather.php:390
msgid "Basic settings"
msgstr "‎הגדרות בסיסיות"

#: owmweather-admin.php:283
msgid "Bypass unit?"
msgstr "‎מעקף יחידה‫?‬"

#: owmweather-admin.php:291
msgid "Unit"
msgstr "‎יחידה"

#: owmweather-admin.php:299
msgid "Bypass date format?"
msgstr "‎מעקב פורמט תאריך‫?‬"

#: owmweather-admin.php:307
msgid "Date"
msgstr "‎תאריך"

#: owmweather-admin.php:316
msgid "Display settings"
msgstr "‎הגדרות תצוגה"

#: owmweather-admin.php:323 owmweather.php:468
msgid "Current weather?"
msgstr "‎מזג אוויר נוכחי‫?‬"

#: owmweather-admin.php:331 owmweather.php:474
msgid "Short condition?"
msgstr "מצב קצר?"

#: owmweather-admin.php:339
msgid "Bypass the todays date format?"
msgstr ""

#: owmweather-admin.php:347
msgid "Todays date or day of the week?"
msgstr ""

#: owmweather-admin.php:355 owmweather.php:552
msgid "Temperatures unit (C / F)?"
msgstr "‎יחידת טמפרטורה ‫(‬פרנהייט ‫/‬ צלזיוס‫)?‬"

#: owmweather-admin.php:363
msgid "Sunrise + sunset?"
msgstr "‎זריחה ‫+‬ שקיעה‫?‬"

#: owmweather-admin.php:371 owmweather-admin.php:724 owmweather.php:510
msgid "Wind?"
msgstr "‎רוח‫?‬"

#: owmweather-admin.php:379
#, fuzzy
#| msgid "Wind?"
msgid "Wind unit?"
msgstr "‎רוח‫?‬"

#: owmweather-admin.php:387 owmweather.php:525
msgid "Humidity?"
msgstr "‎לחות‫?‬"

#: owmweather-admin.php:395 owmweather-admin.php:740 owmweather.php:531
msgid "Pressure?"
msgstr "‎לחץ ברומטרי‫?‬"

#: owmweather-admin.php:403 owmweather.php:537
msgid "Cloudiness?"
msgstr "‎מצב העננים‫?‬"

#: owmweather-admin.php:411 owmweather.php:543
msgid "Precipitation?"
msgstr "משקעים?"

#: owmweather-admin.php:419
msgid "Hour forecast?"
msgstr "‎תחזית שעה‫?‬"

#: owmweather-admin.php:427
msgid "Bypass number of hours forecast settings?"
msgstr "‎מעקף הגדרות מספר שעות התחזית‫?‬"

#: owmweather-admin.php:435
msgid "Number of range hours forecast?"
msgstr "‎טווח שעות התחזית‫?‬"

#: owmweather-admin.php:443 owmweather-admin.php:1192
msgid "Bypass individual temperatures settings?"
msgstr "‎מעקף הגדרות טמפרטורה יחודיות‫?‬"

#: owmweather-admin.php:451 owmweather.php:558
msgid "Today temperature?"
msgstr ""

#: owmweather-admin.php:459
#, fuzzy
#| msgid "16-Day Forecast"
msgid "5-Days Forecast"
msgstr "‎תחזית 16 ימים"

#: owmweather-admin.php:467
msgid "Bypass number of days forecast settings?"
msgstr "מעקף הגדרות מספר ימי התחזית?"

#: owmweather-admin.php:475
msgid "Number of days forecast"
msgstr "מספר ימי התחזית"

#: owmweather-admin.php:483
msgid "Precipitations forecast?"
msgstr ""

#: owmweather-admin.php:491 owmweather-admin.php:1301
msgid "Bypass the length of name days?"
msgstr "מעקף אורך שמות הימים?"

#: owmweather-admin.php:499
msgid "Length name days:"
msgstr ""

#: owmweather-admin.php:507 owmweather.php:623
msgid "Link to OpenWeatherMap?"
msgstr "‎קישור לאתר OpenWeatherMap‫?‬"

#: owmweather-admin.php:515 owmweather.php:631
msgid "Update date?"
msgstr "‎לעדכן תאריך‫?‬"

#: owmweather-admin.php:523
msgid "Fluid design?"
msgstr ""

#: owmweather-admin.php:531
msgid "Default Max width parent container (in pixels)?"
msgstr ""

#: owmweather-admin.php:540
msgid "Advanced settings"
msgstr "‎הגדרות מתקדמות"

#: owmweather-admin.php:547
msgid "CSS 3 Animations"
msgstr "‎אנימציות CSS3"

#: owmweather-admin.php:555 owmweather.php:654
msgid "Background color"
msgstr "‎צבע רקע"

#: owmweather-admin.php:563 owmweather.php:658
msgid "Text color"
msgstr "‎צבע טקסט"

#: owmweather-admin.php:571 owmweather.php:662
msgid "Border color"
msgstr "‏‎צבע מסגרת"

#: owmweather-admin.php:579
msgid "Bypass size?"
msgstr "‎מעקף גודל‫?‬"

#: owmweather-admin.php:587
msgid "Size"
msgstr "‎גודל"

#: owmweather-admin.php:595
msgid "Disable cache"
msgstr "‎לבטל מטמון"

#: owmweather-admin.php:603
msgid "Time cache refresh (in minutes)"
msgstr "זמן רענון מטמון (דקות)"

#: owmweather-admin.php:611
msgid "Open Weather Map API key"
msgstr "מפתח API עבור Open Weather Map"

#: owmweather-admin.php:619
msgid "Disable Bootstrap Modal JS?"
msgstr "לבטל Bootstrap Modal JS?"

#: owmweather-admin.php:629
msgid "Map settings"
msgstr "‎הגדרות מפה"

#: owmweather-admin.php:636
msgid "Map?"
msgstr "‎מפה‫?‬"

#: owmweather-admin.php:644
msgid "Map height"
msgstr "‎גובה מפה"

#: owmweather-admin.php:652
msgid "Bypass layers opacity?"
msgstr "‎מעקף שקיפות שכבות‫?‬"

#: owmweather-admin.php:660 owmweather.php:690
msgid "Layers opacity"
msgstr "‎שקיפות שכבות"

#: owmweather-admin.php:668
msgid "Bypass zoom?"
msgstr "‏‎מעקף הגדלה?‬"

#: owmweather-admin.php:676 owmweather.php:706
msgid "Zoom"
msgstr "הגדלה"

#: owmweather-admin.php:684
msgid "Disable zoom wheel?"
msgstr "‏‎לבטל גלגל הגדלה?‬"

#: owmweather-admin.php:692
msgid "Stations?"
msgstr "‎תחנות‫?‬"

#: owmweather-admin.php:700
msgid "Clouds?"
msgstr "‎עננים‫?‬"

#: owmweather-admin.php:708
msgid "Precipitations?"
msgstr "משקעים?"

#: owmweather-admin.php:716
msgid "Snow?"
msgstr "‎שלג‫?‬"

#: owmweather-admin.php:732
msgid "Temperatures?"
msgstr "‎טמפרטורות‫?‬"

#: owmweather-admin.php:801
msgid "Basic settings to bypass:"
msgstr "‎הגדרות בסיסיות למעקף‫:‬"

#: owmweather-admin.php:806
msgid "Display settings to bypass:"
msgstr "‎הגדרות תצוגה למעקף‫:‬"

#: owmweather-admin.php:811
msgid "Advanced settings to bypass:"
msgstr "‎הגדרות מתקדמות למעקף‫:‬"

#: owmweather-admin.php:816
msgid "Map settings to bypass:"
msgstr "‎הגדרות מפה למעקף‫:‬"

#: owmweather-admin.php:821
msgid "&nbsp;"
msgstr "‏&nbsp;"

#: owmweather-admin.php:837
msgid "Enable bypass unit on all weather?"
msgstr "הפעל מעקף יחידות על כל מזג האוויר?"

#: owmweather-admin.php:853 owmweather.php:416
msgid "Imperial"
msgstr "שיטה אימפריאלית"

#: owmweather-admin.php:856 owmweather.php:417
msgid "Metric"
msgstr "‎שיטה מטרית"

#: owmweather-admin.php:873
msgid "Enable bypass date format on all weather?"
msgstr "הפעל מעקף פורמט תאריך על כל מזג האוויר?"

#: owmweather-admin.php:889 owmweather.php:423
msgid "12 h"
msgstr "‎12 שעות"

#: owmweather-admin.php:892 owmweather.php:424
msgid "24 h"
msgstr "‎24 שעות"

#: owmweather-admin.php:909
msgid "Display current weather on all weather?"
msgstr "‏‎הצג מזג אוויר נוכחי על כל מזג האוויר‫?‬"

#: owmweather-admin.php:924
msgid "Display short condition on all weather?"
msgstr "הצג מצב קצר על כל מזג האוויר?"

#: owmweather-admin.php:939
msgid "Bypass todays date format?"
msgstr ""

#: owmweather-admin.php:955
msgid "None (default)?"
msgstr ""

#: owmweather-admin.php:963 owmweather.php:489
msgid "Day of the week (eg: Sunday)?"
msgstr ""

#: owmweather-admin.php:971
msgid "Todays date?"
msgstr ""

#: owmweather-admin.php:987
msgid "Display temperatures unit (C / F)?"
msgstr "הצג יחידת טמפרטורה (צלזיוס / פרנהייט)?"

#: owmweather-admin.php:1003
msgid "Display sunrise - sunset on all weather?"
msgstr "הצג זריחה - שקיעה על כל מזג האוויר?"

#: owmweather-admin.php:1019
msgid "Display wind on all weather?"
msgstr "הצג רוח על כל מזג האוויר?"

#: owmweather-admin.php:1035
msgid "No bypass"
msgstr ""

#: owmweather-admin.php:1038 owmweather.php:516
msgid "mi/h"
msgstr ""

#: owmweather-admin.php:1041 owmweather.php:517
msgid "m/s"
msgstr ""

#: owmweather-admin.php:1044 owmweather.php:518
msgid "km/h"
msgstr ""

#: owmweather-admin.php:1047 owmweather.php:519
msgid "kt"
msgstr ""

#: owmweather-admin.php:1064
msgid "Display humidity on all weather?"
msgstr "הצג לחות על כל מזג האוויר?"

#: owmweather-admin.php:1080
msgid "Display pressure on all weather?"
msgstr "הצג לחץ ברומטרי על כל מזג האוויר?"

#: owmweather-admin.php:1096
msgid "Display cloudiness on all weather?"
msgstr "הצג עננות על כל מזג האוויר?"

#: owmweather-admin.php:1112
msgid "Display precipitation on all weather?"
msgstr "הצג משקעים על כל מזג האוויר?"

#: owmweather-admin.php:1128
msgid "Display hour forecast on all weather?"
msgstr "הצג תחזית שעתית על כל מזג האוורי?"

#: owmweather-admin.php:1144
msgid "Enable bypass number of hours forecast on all weather?"
msgstr "הפעל מעקף מספר שעות תחזית על כל מזג האוויר?"

#: owmweather-admin.php:1160 owmweather.php:573
msgid "1"
msgstr "1"

#: owmweather-admin.php:1163 owmweather.php:574
msgid "2"
msgstr "2"

#: owmweather-admin.php:1166 owmweather.php:575
msgid "3"
msgstr "3"

#: owmweather-admin.php:1169 owmweather.php:576
msgid "4"
msgstr "4"

#: owmweather-admin.php:1172 owmweather.php:577
msgid "5"
msgstr "5"

#: owmweather-admin.php:1175 owmweather.php:578
msgid "6"
msgstr "6"

#: owmweather-admin.php:1208
msgid "Display today temperature on all weather?"
msgstr ""

#: owmweather-admin.php:1224
#, fuzzy
#| msgid "Display 16-day Forecast on all weather?"
msgid "Display 5-days Forecast on all weather?"
msgstr "הצג 16 ימי תחזית על כל מזג האוויר?"

#: owmweather-admin.php:1240
msgid "Enable bypass number of days forecast on all weather?"
msgstr "הפעל מעקף מספר ימי התחזית על כל מזג האוויר?"

#: owmweather-admin.php:1256 owmweather.php:595
msgid "1 day"
msgstr "‏‎יום אחד"

#: owmweather-admin.php:1259 owmweather.php:596
msgid "2 days"
msgstr "‎2 ימים"

#: owmweather-admin.php:1262 owmweather.php:597
msgid "3 days"
msgstr "‎3 ימים"

#: owmweather-admin.php:1265 owmweather.php:598
msgid "4 days"
msgstr "‎4 ימים"

#: owmweather-admin.php:1268 owmweather.php:599
msgid "5 days"
msgstr "‎5 ימים"

#: owmweather-admin.php:1285
msgid "Display Forecast Precipitations on all Weather?"
msgstr ""

#: owmweather-admin.php:1318
msgid "Short days names"
msgstr "‎שמות ימים קצרים"

#: owmweather-admin.php:1326
msgid "Normal days names"
msgstr "‎שמות ימים רגילים"

#: owmweather-admin.php:1342
msgid "Display link to full weather on OpenWeatherMap?"
msgstr "הצג קישור למזג אוויר מלא באתר OpenWeatherMap?"

#: owmweather-admin.php:1359
msgid "Display update date on all weather?"
msgstr "הצג עדכון תאריך על כל מזג האוויר?"

#: owmweather-admin.php:1376
msgid "Enable fluid design on all weather?"
msgstr ""

#: owmweather-admin.php:1401
msgid "Disable CSS3 animations, transformations and transitions?"
msgstr "בטל אנימציות CSS3, אפקטים ומעברים?"

#: owmweather-admin.php:1446
msgid "Enable bypass size on all weather?"
msgstr "הפעל מעקף גודל על כל מזג האוויר?"

#: owmweather-admin.php:1462 owmweather.php:672
msgid "Small"
msgstr "‎קטן"

#: owmweather-admin.php:1465 owmweather.php:673
msgid "Medium"
msgstr "‎בינוני"

#: owmweather-admin.php:1468 owmweather.php:674
msgid "Large"
msgstr "‎גדול"

#: owmweather-admin.php:1485
msgid "Disable weather cache? (not recommended)"
msgstr "בטל מטמון מזג אוויר? (לא מומלץ)"

#: owmweather-admin.php:1498
msgid "Default value: 30 minutes"
msgstr "ערך ברירת מחדל: 30 דקות"

#: owmweather-admin.php:1507
msgid "Strongly recommended: "
msgstr ""

#: owmweather-admin.php:1507
msgid "Strongly recommended: get your free OWM API key here"
msgstr ""

#: owmweather-admin.php:1520
msgid ""
"Disable Bootstrap Modal JS? (disable this if you already include Bootstrap JS "
"in your theme)"
msgstr "בטל Bootstrap Modal JS? (בטל אם כבר כללת סקריפט זה בערכת העיצוב שלך)"

#: owmweather-admin.php:1536
msgid "Enable map on all weather?"
msgstr "הפעל מפה על כל מזג האוויר?"

#: owmweather-admin.php:1561
msgid "Enable bypass map opacity on all weather?"
msgstr "הפעל מעקף שקיפות מפה על כל מזג האוויר?"

#: owmweather-admin.php:1624
msgid "Enable bypass map zoom on all weather?"
msgstr "הפעל מעקף הגדלת מפה על כל מזג האוויר?"

#: owmweather-admin.php:1708
msgid "Disable zoom wheel on all weather?"
msgstr "בטל גלגל הגדלה על כל מזג האוויר?"

#: owmweather-admin.php:1724
msgid "Display stations on all weather maps?"
msgstr "הצג תחנות על כל מפות מזג האוויר?"

#: owmweather-admin.php:1740
msgid "Display clouds on all weather maps?"
msgstr "הצג עננים על כל מפות מזג האוויר?"

#: owmweather-admin.php:1756
msgid "Display precipitations on all weather maps?"
msgstr "הצג משקעים על כל מפות מזג האוויר?"

#: owmweather-admin.php:1773
msgid "Display snow on all weather maps?"
msgstr "הצג שלג על כל מפות מזג האוויר?"

#: owmweather-admin.php:1789
msgid "Display wind on all weather maps?"
msgstr "הצג רוח על כל מפות מזג האוויר?"

#: owmweather-admin.php:1805
msgid "Display temperatures on all weather maps?"
msgstr "הצג טמפרטורה על כל מפות מזג האוויר?"

#: owmweather-admin.php:1821
msgid "Display pressure on all weather maps?"
msgstr "הצג לחץ ברומטרי על כל מפות מזג האוויר?"

#: owmweather-admin.php:1831
msgid "Problem with OWM Weather?"
msgstr "יש לך בעיה עם OWM Weather?"

#: owmweather-admin.php:1832
msgid "FAQ"
msgstr "‎שאלות נפוצות"

#: owmweather-admin.php:1832
msgid "Read our FAQ"
msgstr "‎קרא את השאלות הנפוצות"

#: owmweather-admin.php:1833
msgid "Guides"
msgstr "‎מדריכים"

#: owmweather-admin.php:1833
msgid "Read our Guides"
msgstr "‎קרא את המדריכים"

#: owmweather-admin.php:1834
msgid "OWM Weather Forum on WordPress.org"
msgstr "פורום OWM Weather באתר וורדפרס"

#: owmweather-admin.php:1851
msgid "Setup OWM Weather"
msgstr ""

#: owmweather-admin.php:1852
msgid "Follow this video to setup OWM Weather:"
msgstr ""

#: owmweather-admin.php:1856
msgid "Create your first weather"
msgstr ""

#: owmweather-admin.php:1857
msgid "Follow this video to create your first weather with OWM Weather:"
msgstr ""

#: owmweather-export.php:40
msgid "Please upload a valid .json file"
msgstr "העלה קובץ json תקני בבקשה"

#: owmweather-export.php:44
msgid "Please upload a file to import"
msgstr "העלה קובץ לייבוא בבקשה"

#: owmweather-pointers.php:32
msgid ""
"<strong>Before starting:</strong> enter your own OpenWeatherMap API key in"
msgstr ""

#: owmweather-pointers.php:32
msgid "Advanced Settings!"
msgstr ""

#: owmweather-widget.php:32
msgid "Please select a weather via configure link"
msgstr ""

#: owmweather-widget.php:55
msgid "Select the weather to display:"
msgstr "בחר את מזג האוויר להצגה:"

#: owmweather.php:61
msgid "Settings"
msgstr ""

#: owmweather.php:294
msgid "Duplicate this item"
msgstr ""

#: owmweather.php:294
msgid "Duplicate"
msgstr ""

#: owmweather.php:307
msgid "OWM Weather global settings"
msgstr ""

#: owmweather.php:312
msgid "Copy and paste this shortcode anywhere in posts, pages, text widgets: "
msgstr "העתק והדבק את הקיצור הזה בכל מקום בפוסטים, עמודים או ווידג׳טים: "

#: owmweather.php:320
msgid ""
"If you need to display this weather anywhere in your theme, simply copy and "
"paste this code snippet in your PHP file like sidebar.php: "
msgstr ""
"אם אתה צריך להציג את מזג האוויר הזה בכל מקום בערכת העיצוב שלך, פשוט העתק "
"והדבק את הקוד הזה בתוך קובץ PHP בערכת העיצוב: "

#: owmweather.php:398
msgid "City"
msgstr "‎עיר"

#: owmweather.php:399
msgid "Enter your city"
msgstr "‎הכנס את העיר שלך"

#: owmweather.php:402
msgid "Custom city title"
msgstr "כותרת עיר מותאמת-אישית:"

#: owmweather.php:406
msgid "State?"
msgstr "‎מדינה"

#: owmweather.php:410
msgid "Country?"
msgstr "‎ארץ"

#: owmweather.php:414
msgid "Imperial or metric units?"
msgstr "יחידות אימפריאליות או מטריות?"

#: owmweather.php:421
msgid "12h / 24h date format?"
msgstr "פורמט שעה 12 שעות / 24 שעות?"

#: owmweather.php:430
msgid "Bypass default WordPress timezone setting?"
msgstr ""

#: owmweather.php:434
msgid "Custom timezone? (default: WordPress general settings)"
msgstr ""

#: owmweather.php:436
msgid "UTC -12"
msgstr ""

#: owmweather.php:437
msgid "UTC -11"
msgstr ""

#: owmweather.php:438
msgid "UTC -10"
msgstr ""

#: owmweather.php:439
msgid "UTC -9"
msgstr ""

#: owmweather.php:440
msgid "UTC -8"
msgstr ""

#: owmweather.php:441
msgid "UTC -7"
msgstr ""

#: owmweather.php:442
msgid "UTC -6"
msgstr ""

#: owmweather.php:443
msgid "UTC -5"
msgstr ""

#: owmweather.php:444
msgid "UTC -4"
msgstr ""

#: owmweather.php:445
msgid "UTC -3"
msgstr ""

#: owmweather.php:446
msgid "UTC -2"
msgstr ""

#: owmweather.php:447
msgid "UTC -1"
msgstr ""

#: owmweather.php:448
msgid "UTC 0"
msgstr ""

#: owmweather.php:449
msgid "UTC +1"
msgstr ""

#: owmweather.php:450
msgid "UTC +2"
msgstr ""

#: owmweather.php:451
msgid "UTC +3"
msgstr ""

#: owmweather.php:452
msgid "UTC +4"
msgstr ""

#: owmweather.php:453
msgid "UTC +5"
msgstr ""

#: owmweather.php:454
msgid "UTC +6"
msgstr ""

#: owmweather.php:455
msgid "UTC +7"
msgstr ""

#: owmweather.php:456
msgid "UTC +8"
msgstr ""

#: owmweather.php:457
msgid "UTC +9"
msgstr ""

#: owmweather.php:458
msgid "UTC +10"
msgstr ""

#: owmweather.php:459
msgid "UTC +11"
msgstr ""

#: owmweather.php:460
msgid "UTC +12"
msgstr ""

#: owmweather.php:478
msgid "Dates"
msgstr ""

#: owmweather.php:483
msgid "No date (default)?"
msgstr ""

#: owmweather.php:495
msgid "Todays date (based on your WordPress General Settings)?"
msgstr ""

#: owmweather.php:499
msgid "Misc"
msgstr ""

#: owmweather.php:504
msgid "Sunrise + sunset? appears only if today date is checked"
msgstr ""

#: owmweather.php:514
msgid "Wind unit: "
msgstr ""

#: owmweather.php:547
msgid "Temperatures"
msgstr "‎טמפרטורות"

#: owmweather.php:562
msgid "Hourly Forecast"
msgstr "תחזית שעתית"

#: owmweather.php:567
msgid "Hour Forecast?"
msgstr "תחזית שעה?"

#: owmweather.php:571
msgid "How hours ranges ?"
msgstr "טווח שעות?"

#: owmweather.php:581
msgid ""
"Make sure you have properly set the date of your site in WordPress settings."
msgstr "וודא שהתאריך הנכון קבוע בהגדרות וורדפרס."

#: owmweather.php:584
#, fuzzy
#| msgid "16-Day Forecast"
msgid "5-Day Forecast"
msgstr "‎תחזית 16 ימים"

#: owmweather.php:589
#, fuzzy
#| msgid "16-Day Forecast? (today + 15 days max)"
msgid "5-Day Forecast? (today + 5 days max)"
msgstr "תחזית 16 ימים? (היום + 15 יום מקסימום)"

#: owmweather.php:593
msgid "How many days?"
msgstr "כמה ימים?"

#: owmweather.php:605
msgid "Forecast Precipitations?"
msgstr ""

#: owmweather.php:611
msgid "Short days names?"
msgstr "שמות ימים קצרים?"

#: owmweather.php:617
msgid "Normal days names?"
msgstr "שמות ימים רגילים?"

#: owmweather.php:638
msgid "Fluid design? (useful with small container)"
msgstr ""

#: owmweather.php:642
msgid "Max width parent container to enable fluid design (pixels):"
msgstr ""

#: owmweather.php:650
msgid "Disable CSS3 animations?"
msgstr "בטל אנימציות CSS3?"

#: owmweather.php:666
msgid "Custom CSS"
msgstr "‏CSS מותאם-אישית"

#: owmweather.php:670
msgid "Weather size?"
msgstr "גודל מזג אוויר?"

#: owmweather.php:682
msgid "Display map?"
msgstr "הצג מפה?"

#: owmweather.php:686
msgid "Map height (in px)"
msgstr "גובה מפה (בפיקסלים)"

#: owmweather.php:731
msgid "Disable zoom wheel on map?"
msgstr "בטל גלגל הגדלה במפה?"

#: owmweather.php:735
msgid "Layers"
msgstr "שכבות"

#: owmweather.php:740
msgid "Display stations?"
msgstr "הצג תחנות?"

#: owmweather.php:746
msgid "Display clouds?"
msgstr "הצג עננים?"

#: owmweather.php:752
msgid "Display precipitation?"
msgstr "הצג משקעים?"

#: owmweather.php:758
msgid "Display snow?"
msgstr "הצג שלג?"

#: owmweather.php:764
msgid "Display wind?"
msgstr "הצג רוח?"

#: owmweather.php:770
msgid "Display temperature?"
msgstr "הצג טמפרטורה?"

#: owmweather.php:776
msgid "Display pressure?"
msgstr "הצג לחץ ברומטרי?"

#: owmweather.php:1407 owmweather.php:1415 owmweather.php:1425 owmweather.php:1486
#: owmweather.php:1493 owmweather.php:1501
msgid "Unable to retrieve weather data"
msgstr "לא ניתן לקבל מידע אודות מזג האוויר"

#: owmweather.php:1582
msgid "Full weather on OpenWeatherMap"
msgstr "תחזית מלאה באתר OpenWeatherMap"

#: owmweather.php:1582
msgid "Full weather"
msgstr "תחזית מלאה"

#: owmweather.php:1585
msgid "Last update: "
msgstr "עדכון אחרון: "

#: owmweather.php:1617 owmweather.php:1705 owmweather.php:1730
msgid "Sunday"
msgstr "‎ראשון"

#: owmweather.php:1620 owmweather.php:1708 owmweather.php:1733
msgid "Monday"
msgstr "‎שני"

#: owmweather.php:1623 owmweather.php:1711 owmweather.php:1736
msgid "Tuesday"
msgstr "‎שלישי"

#: owmweather.php:1626 owmweather.php:1714 owmweather.php:1739
msgid "Wednesday"
msgstr "‎רביעי"

#: owmweather.php:1629 owmweather.php:1717 owmweather.php:1742
msgid "Thursday"
msgstr "‎חמישי"

#: owmweather.php:1632 owmweather.php:1720 owmweather.php:1745
msgid "Friday"
msgstr "‎שישי"

#: owmweather.php:1635 owmweather.php:1723 owmweather.php:1748
msgid "Saturday"
msgstr "‎שבת"

#: owmweather.php:1680
msgid "Sun"
msgstr "‎א׳"

#: owmweather.php:1683
msgid "Mon"
msgstr "‎ב׳"

#: owmweather.php:1686
msgid "Tue"
msgstr "‎ג׳"

#: owmweather.php:1689
msgid "Wed"
msgstr "‎ד׳"

#: owmweather.php:1692
msgid "Thu"
msgstr "‎ה׳"

#: owmweather.php:1695
msgid "Fri"
msgstr "‎ו׳"

#: owmweather.php:1698
msgid "Sat"
msgstr "‎ש׳"

#: owmweather.php:1777
msgid "clear sky"
msgstr ""

#: owmweather.php:1782
msgid "few clouds"
msgstr ""

#: owmweather.php:1787
msgid "scattered clouds"
msgstr ""

#: owmweather.php:1792
msgid "broken clouds"
msgstr ""

#: owmweather.php:1797
msgid "overcast clouds"
msgstr ""

#: owmweather.php:1804
msgid "light rain"
msgstr ""

#: owmweather.php:1809
msgid "moderate rain"
msgstr ""

#: owmweather.php:1814
msgid "heavy intensity rain"
msgstr ""

#: owmweather.php:1819
msgid "very heavy rain"
msgstr ""

#: owmweather.php:1824
msgid "extreme rain"
msgstr ""

#: owmweather.php:1829
msgid "freezing rain"
msgstr ""

#: owmweather.php:1834
msgid "light intensity shower rain"
msgstr ""

#: owmweather.php:1839
msgid "shower rain"
msgstr ""

#: owmweather.php:1844
msgid "heavy intensity shower rain"
msgstr ""

#: owmweather.php:1849
msgid "ragged shower rain"
msgstr ""

#: owmweather.php:1856
msgid "light intensity drizzle"
msgstr ""

#: owmweather.php:1861
msgid "drizzle"
msgstr ""

#: owmweather.php:1866
msgid "heavy intensity drizzle"
msgstr ""

#: owmweather.php:1871
msgid "light intensity drizzle rain"
msgstr ""

#: owmweather.php:1876
msgid "drizzle rain"
msgstr ""

#: owmweather.php:1881
msgid "heavy intensity drizzle rain"
msgstr ""

#: owmweather.php:1886
msgid "shower rain and drizzle"
msgstr ""

#: owmweather.php:1891
msgid "heavy shower rain and drizzle"
msgstr ""

#: owmweather.php:1896
msgid "shower drizzle"
msgstr ""

#: owmweather.php:1903
msgid "light snow"
msgstr ""

#: owmweather.php:1908
msgid "snow"
msgstr ""

#: owmweather.php:1913
msgid "heavy snow"
msgstr ""

#: owmweather.php:1918
msgid "sleet"
msgstr ""

#: owmweather.php:1923
msgid "shower sleet"
msgstr ""

#: owmweather.php:1928
msgid "light rain and snow"
msgstr ""

#: owmweather.php:1933
msgid "rain and snow"
msgstr ""

#: owmweather.php:1938
msgid "light shower snow"
msgstr ""

#: owmweather.php:1943
msgid "shower snow"
msgstr ""

#: owmweather.php:1948
msgid "heavy shower snow"
msgstr ""

#: owmweather.php:1955
msgid "mist"
msgstr ""

#: owmweather.php:1960
msgid "smoke"
msgstr ""

#: owmweather.php:1965
msgid "haze"
msgstr ""

#: owmweather.php:1970
msgid "sand, dust whirls"
msgstr ""

#: owmweather.php:1975
msgid "fog"
msgstr ""

#: owmweather.php:1982
msgid "tornado"
msgstr ""

#: owmweather.php:1987
msgid "tropical storm"
msgstr ""

#: owmweather.php:1992
msgid "hurricane"
msgstr ""

#: owmweather.php:1997
msgid "windy"
msgstr ""

#: owmweather.php:2002
msgid "hail"
msgstr ""

#: owmweather.php:2009
msgid "thunderstorm with light rain"
msgstr ""

#: owmweather.php:2015
msgid "thunderstorm with rain"
msgstr ""

#: owmweather.php:2021
msgid "thunderstorm with heavy rain"
msgstr ""

#: owmweather.php:2027
msgid "light thunderstorm"
msgstr ""

#: owmweather.php:2033
msgid "thunderstorm"
msgstr ""

#: owmweather.php:2039
msgid "heavy thunderstorm"
msgstr ""

#: owmweather.php:2045
msgid "ragged thunderstorm"
msgstr ""

#: owmweather.php:2051
msgid "thunderstorm with light drizzle"
msgstr ""

#: owmweather.php:2057
msgid "thunderstorm with drizzle"
msgstr ""

#: owmweather.php:2063
msgid "thunderstorm with heavy drizzle"
msgstr ""

#: owmweather.php:2095
msgid "Today"
msgstr "‎היום"

#: owmweather.php:2145
msgid "Wind"
msgstr "‎רוח"

#: owmweather.php:2148
msgid "Humidity"
msgstr "‎לחות"

#: owmweather.php:2151
msgid "Pressure"
msgstr "לחץ ברומטרי"

#: owmweather.php:2154
msgid "Cloudiness"
msgstr "‎עננות"

#: owmweather.php:2158 owmweather.php:2163
msgid "Precipitation"
msgstr "משקעים"

#: owmweather.php:2172 owmweather.php:2180
msgid "Now"
msgstr "‎עכשיו"

#: owmweather.php:2238 owmweather.php:2260
msgid "0 mm"
msgstr ""

#: owmweather.php:2685
msgid "Shortcode"
msgstr "קיצור"

#: owmweather.php:2708 owmweather.php:2709 owmweather.php:2710
msgid "Weather"
msgstr "‎מזג אוויר"

#: owmweather.php:2711
msgid "Parent Weather:"
msgstr "מזג אוויר אם:"

#: owmweather.php:2712
msgid "All Weather"
msgstr "כל מזג האוויר"

#: owmweather.php:2713
msgid "View Weather"
msgstr "הצג מזג אוויר"

#: owmweather.php:2714
msgid "Add New Weather"
msgstr "הוסף מזג אוויר חדש"

#: owmweather.php:2715
msgid "New Weather"
msgstr "מזג אוויר חדש"

#: owmweather.php:2716
msgid "Edit Weather"
msgstr "ערוך מזג אוויר"

#: owmweather.php:2717
msgid "Update Weather"
msgstr "עדכן מזג אוויר"

#: owmweather.php:2718
msgid "Search Weather"
msgstr "חפש מזג אוויר"

#: owmweather.php:2719
msgid "No weather found"
msgstr "לא מצאתי מזג אוויר"

#: owmweather.php:2720
msgid "No weather found in Trash"
msgstr "לא מצאתי מזג אוויר בפח"

#: owmweather.php:2724
msgid "weather"
msgstr "‎מזג אוויר"

#: owmweather.php:2725
msgid "Listing weather"
msgstr "רשימת מזג אוויר"

#: owmweather.php:2762 owmweather.php:2765
msgid " updated."
msgstr "‏ ‎עודכן."

#: owmweather.php:2763
msgid "Custom field updated."
msgstr "שדה מותאם-אישית עודכן."

#: owmweather.php:2764
msgid "Custom field deleted."
msgstr "שדה מותאים-אישית נמחק."

#: owmweather.php:2766
#, php-format
msgid " restored to revision from %s"
msgstr " שוחזר לגירסא מתוך %s"

#: owmweather.php:2767
msgid " published."
msgstr " ‏‎פורסם."

#: owmweather.php:2768
msgid "Page saved."
msgstr "‎העמוד נשמר‫.‬"

#: owmweather.php:2769
msgid " submitted."
msgstr " הוגש."

#: owmweather.php:2770
#, php-format
msgid " scheduled for: <strong>%1$s</strong>. "
msgstr " נקבע ל: <strong>%1$s</strong>. "

#: owmweather.php:2770
msgid "M j, Y @ G:i"
msgstr "‏M j, Y @ G:i"

#: owmweather.php:2771
msgid " draft updated."
msgstr " טיוטה עודכנה."

#: owmweather.php:2787
msgid ""
"OWM Weather: Please enter your own OpenWeatherMap API key to avoid limits "
"requests."
msgstr ""

#~ msgid "6 days"
#~ msgstr "‎6 ימים"

#~ msgid "7 days"
#~ msgstr "‎7 ימים"

#~ msgid "8 days"
#~ msgstr "‎8 ימים"

#~ msgid "9 days"
#~ msgstr "‎9 ימים"

#~ msgid "10 days"
#~ msgstr "‎10 ימים"

#~ msgid "11 days"
#~ msgstr "‎11 ימים"

#~ msgid "12 days"
#~ msgstr "‎12 ימים"

#~ msgid "13 days"
#~ msgstr "‎13 ימים"

#~ msgid "14 days"
#~ msgstr "‎14 ימים"

#~ msgid "15 days"
#~ msgstr "‎15 ימים"

#~ msgid "Load JS/CSS from:"
#~ msgstr "‎לטעון JS/CSS מ‫:‬"

#~ msgid "Lenght name days:"
#~ msgstr "‎אורך שמות הימים‫:‬"

#~ msgid "Not just another WordPress Weather plugin!"
#~ msgstr "‎לא סתם עוד תוסף מזג אוויר לוורדפרס‫!‬"

#~ msgid "Plugin website"
#~ msgstr "‎אתר התוסף"

#~ msgid "Follow us on Twitter!"
#~ msgstr "‎עקוב אחרינו בטוויטר‫!‬"

#~ msgid "OWM Weather Geolocation"
#~ msgstr "OWM Weather גיאו‫-‬מיקום"

#~ msgid "$ 39"
#~ msgstr "$39"

#~ msgid "Geolocated weather for your visitors."
#~ msgstr "‎הצגת מזג אוויר על בסיס מיקום הגולשים שלך‫.‬"

#~ msgid "Learn more"
#~ msgstr "‎קרא עוד"

#~ msgid "OWM Weather Skins"
#~ msgstr "‎ערכות עיצוב OWM Weather"

#~ msgid "$ 10"
#~ msgstr "$10"

#~ msgid "10 beautiful skins for your weather."
#~ msgstr "‎10 ערכות עיצוב יפות למזג האוויר שלך‫.‬"

#~ msgid "Today date?"
#~ msgstr "תאריך היום?"

#~ msgid "Forum"
#~ msgstr "‎פורום"

#~ msgid "OWM Weather Forum"
#~ msgstr "פורום OWM Weather"

#~ msgid "Bypass language?"
#~ msgstr "‎מעקף שפה‫?‬"

#~ msgid "Language"
#~ msgstr "‎שפה"

#~ msgid "Today date + Temperatures?"
#~ msgstr "‎תאריך היום ‫+‬ טמפרטורות‫?‬"

#~ msgid "Today date + current temperature"
#~ msgstr "‎תאריך היום ‫+‬ טמפרטורה נוכחית"

#~ msgid "Enable bypass language on all weather?"
#~ msgstr "הפעל מעקף שפה על כל מזג האוויר?"

#~ msgid "French"
#~ msgstr "‎צרפתית"

#~ msgid "English"
#~ msgstr "‎אנגלית"

#~ msgid "Russian"
#~ msgstr "‎רוסית"

#~ msgid "Italian"
#~ msgstr "‎איטלקית"

#~ msgid "Spanish"
#~ msgstr "‎ספרדית"

#~ msgid "Ukrainian"
#~ msgstr "‎אוקראינית"

#~ msgid "German"
#~ msgstr "‎גרמנית"

#~ msgid "Portuguese"
#~ msgstr "‎פורטוגזית"

#~ msgid "Romanian"
#~ msgstr "‎רומנית"

#~ msgid "Polish"
#~ msgstr "‎פולנית"

#~ msgid "Finnish"
#~ msgstr "‎פינית"

#~ msgid "Dutch"
#~ msgstr "‎הולנדית"

#~ msgid "Bulgarian"
#~ msgstr "‎בולגרית"

#~ msgid "Swedish"
#~ msgstr "‎שוודית"

#~ msgid "Chinese Traditional"
#~ msgstr "‎סינית מסורתית"

#~ msgid "Chinese Simplified"
#~ msgstr "‎סינית"

#~ msgid "Turkish"
#~ msgstr "‎טורקית"

#~ msgid "Czech"
#~ msgstr "‎צ׳כית"

#~ msgid "Galician"
#~ msgstr "‎גליציאנית"

#~ msgid "Vietnamese"
#~ msgstr "‎ויאטנמית"

#~ msgid "Arabic"
#~ msgstr "‎ערבית"

#~ msgid "Macedonian"
#~ msgstr "‎מקדונית"

#~ msgid "Slovak"
#~ msgstr "‎סלובקית"

#~ msgid "Learn more about how translations works"
#~ msgstr "‎קרא עוד על איך תרגומים עובדים"

#~ msgid "Display today date + temperatures on all weather?"
#~ msgstr "הצג תאריך היום + טמפרטורות על כל מזג האוויר?"

#~ msgid "Display Today date on all weather?"
#~ msgstr "הצג את תאריך היום על כל מזג האוויר?"

#~ msgid "Display language?"
#~ msgstr "הצג שפה?"

#~ msgid "Date + temperatures?"
#~ msgstr "תאריך + טמפרטורות?"

#~ msgid "Sunrise + sunset? appears only if date + temperatures is checked"
#~ msgstr "זריחה + שקיעה? מוצג רק אם תאריך + טמפרטורות נבחר"

#~ msgid "Import/Export"
#~ msgstr "‎ייבוא‫/‬ייצוא"
