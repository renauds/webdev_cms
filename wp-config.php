<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'monsite' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'keUs,.hf_1y-6R1@5LLbH<IZV`$p!4o:9*HP-vnYNI1fsQmwJIxI#xBu&LUH76p=' );
define( 'SECURE_AUTH_KEY',  'bJbPL#;f*jmvOG1i+ek`D&v!`(f+|_O38a<.Z2|dx`ZG+9`~rrwuf!EEMZk+:QnQ' );
define( 'LOGGED_IN_KEY',    'j%V?w?k3c~E=r4]@eZ^:|[MG6;x2g{KrH.`$$cK=Uvs?(/n%F6?$|^1UG{0aM`Pe' );
define( 'NONCE_KEY',        'kxJ@~w[XYm!C!NnIi9QJ~|=vC^ H[nwu~OyS(/Zb@zoS$[hk|o`Dg!!<w8*VGYdU' );
define( 'AUTH_SALT',        ',w,^!ykNOS-Vx[oz^W2 78)h9YO>WQw>AVP~ho|_;n~f^)6Q=fs{hC+ER*W18Nc<' );
define( 'SECURE_AUTH_SALT', 'A#tW^NZcx1iYt3<%Up^J&MT_;eJ1HIABkTGw/4fV|ASlVy45tb@<O$QmRc}.WSq`' );
define( 'LOGGED_IN_SALT',   'z=W;E<]-}<Vkx/LnKBv}WqE)Phj99_${X[:GotMkHAhN[U3;/`su:W+D wjx{ypY' );
define( 'NONCE_SALT',       'e&A`$ @D8?Iwbcbp<,Db@,3${V|+S6qY:Ie]j5CDQr:fyGtw1>sYP#zu[zN+P)?}' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
